## What is this repository.

This is a repository is a template for a generic LHCb anaylsis.
It includes support for ROOT and RooFit but also matplotlib and scikit-lean, EOS access, snakemake, Docker, gitlab CI runner and SWAN.

_Author: L. Pescatore_

Contact: luca.pescatore@cern.ch

## How to setup the repository

**Please do not clone the original but fork it first!** (which means make a copy of it under your user).
To do it just push the fork button from the main repository page.

Then clone the repository. N.B.: "--recursive" is important because it also clones submodules.

```git clone --recursive ssh://git@gitlab.cern.ch:7999/YOUR USERNAME/LHCb-Analysis-Template.git```

If you want to run interactively in testing mode.

```source setup/setup.sh```

If you want to use snakemake.

```source setup/setup.sh snake ```

If no cvmfs is available

```source setup/setup.sh venv ```

in the latter case you'll have to take care that ROOT and all needed libraries are installed. For example this is done in the Docker image which can be created for this repository (see Docker folder).

#### First installation

For this project to work properly you have to setup two virtual environments: venv and snake.

- snake: N.B.: This needs to be done **before** sourcing scripts/setup.sh.

``` source setup/install_snake.sh```

- venv : Should be automatically installed the first time you run the setup.

If your are somewhere with CVMFS :

```source setup/setup.sh ```

Otherwise:

```source setup/setup.sh venv ```

#### Runner

The CI for this repo runs on an image on a private virtual machine. So you have to enable it.
After you forked follow these instructions:

* Settings -> CI/CD -> Runner Settings
* Click "Enable for this project" for the "analysisrunner" runner.



# Environment

* After the setup the `ANAROOT` variable is avilable pointing to the top folder.
* ROOT, matplotlib, sklearn, etc are setup.
* Your modules should go into the `python/analysis` folder (see later "The analysis package")


## Repository Structure

* The analysis code after stripping should be fully contained into the `python` folders. In particular 
    1. `python/scripts` should contain files intendended to be run inderactively 
    2. `python/analysis` modules intended to be imported.

N.B.: If you make subfolders of `python/analysis` put an empty `__init__.py` file inside each one.
* `output`: this folder should contain the lightweight outputs of your analysis
    1. `output/tables` is where you should put the output tables. 
    2. `output/plots` is where you should put output plots
    3. `output/logs` is where you should put output plots
* `templates` folder which should contain latex templates to be filled automatically.


### The analysis package

This module loads the python environment: `import analysis as an`.

What will this do for you:

* Checking that you sourced the setup.sh file.

* Loading the LHCb style for plots.

* Make the cuts defined available to all python scripts. __All cuts should be defined into analysis/cuts!!__

* Make the locations easily available to you though the `loc` object. Already defined locations the following, feel free to define more as you need them:

    - loc.ROOT   = $ANAROOT
    - loc.PYTHON = $ANAROOT/python/
    - loc.LHCB   = $ANAROOT/LHCb/
    - loc.PLOTS  = $ANAROOT/output/plots/
    - loc.TABS   = $ANAROOT/output/tables/
    - loc.TMPS   = $ANAROOT/templates/

* Provide a common database saved on disk (still needs handling of more people working at same time).

```
import analysis as an
print an.db
{'Test':True, ...}
an.db['myeff'] = 0.99 ## Add a value
an.dumpDB(an.db) ## Saves it to disk in human readable CSV format
```

* Provide easy template handling

Crate a file in the `templates` folder. You can write whatever you want into it just put the values to substitute into curly brackets
   
E.g.: `seleff = ${sel_eff} \pm {sel_eff_err}$`
   
And then use the `db` object to fill it!!

```import anaylsis as an
an.outfile.fill_template("efficiencies","eff_tmp",an.db)```
 
This will look for the keys into the db, fill them into your template and same everything to $ANAROOT/outputs/tables/efficiency.txt



### Access data

Raw data should be put on EOS. A function is provided to ls EOS from anywhere XRootD is installed:

```from pyutils.scripts import remotels
files = remotels.remotels(SOME LOCATION ON EOS)```

It is highly recommended to save the locations of your datasets into a dictionary with labels
for easy retrieval. An example of this can be seen in python/analyisis/locations.py.


## Snakemake

Snakemake is a workload manager that allows you to automatise your offline anaylsis (or parts of it)
See the official tutorial [at this link](https://snakemake.bitbucket.io/snakemake-tutorial.html) (start from Basics Step 1)
Snamekame tutorial by a LHCb user: [starterkit tutorial](https://github.com/lhcb/starterkit-lessons/blob/snakemake/second-analysis-steps/analysis-automation-snakemake.md).

To run snakemake **from a clean shell**:

```
source setup/setup.ch snake
snakemake
```

## Docker

Docker support is available for this repository to allow running the analysis on any machine anywhere in the world (with internet). 
Please have a look at the readme inside the Docker folder.

## Common utilities

The `pyutils` folder contains utilities which you may find useful. See pyutils/README.md.


## Using SWAN (Thanks to Guido Andreassi)
[SWAN](swan.cern.ch) is the CERN tool for interactive development in [jupiter](http://jupyter.org).

To be able to use SWAN, a few dedicated steps are necessary.
1. On lxplus, go to your CERNBOX user folder: /eos/user/<first_letter_of_username>/<username>/
2. Clone this repository (NB: the following procedure currently works only if you clone the repository from the location in 1.)
3. Open a new SWAN session from https://swan.cern.ch , choosing LCG87, gcc49 and no environment script, or whatever is compatible with your analysis
4. Open a new terminal, using the dropdown menu on the top right
5. cd into the repository's folder
6. Run ```source setup/setup-swan.sh```. This script is going to compile the tools and install the dependencies. It might take a fairly long time, but it's needed only the very first time (except if you accidentally delete setup/run-swan.sh, or move the repository, or clone it from scratch).
7. Close the SWAN session
8. Open a new one with the same caracteristics as before, but this time specify ```$CERNBOX_HOME/{your repo}/setup/run-swan.sh```
as environment script
9. Enjoy!

The instructions up to 7. included will need to be run only the first time, as a setup, while 8. and 9. are necessary every time you want to start a new SWAN session. 
