#author: Guido Andreassi <guido.andreassi@cern.ch>
#!/bin/bash  
source /cvmfs/lhcb.cern.ch/group_login.sh

export OLD_PATH=$PATH
export OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH

export REPO_ROOT="$(dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ))"
echo "REPO_ROOT:"
echo $REPO_ROOT
if [ "$1" != "" ]; then
    export REPO_ROOT="$1"
fi

export LOCAL=$CERNBOX_HOME/.local/$(basename $REPO_ROOT)
export PYTHON_LOCAL=$LOCAL/lib/python2.7/site-packages/
export PATH=$LOCAL/bin:$PATH
export PYTHONPATH=$PYTHON_LOCAL:$PYTHONPATH


echo "Setting up environment for Swan. Be patient..."
pip install --user --upgrade pip
pip install --install-option="--prefix=$LOCAL" -I --no-cache-dir -r $REPO_ROOT/python/venv_requirements.txt
echo "Finished installing dependencies."

echo 'Creating run script...'
export outfile=$REPO_ROOT/setup/run-swan.sh
echo 'export REPO_ROOT='$REPO_ROOT > $outfile #to retrieve the path to the analysis repo
echo 'source /cvmfs/lhcb.cern.ch/group_login.sh' >> $outfile
echo 'export OLD_PATH=$PATH' >> $outfile
echo 'export OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH' >> $outfile
echo 'source $REPO_ROOT/setup/setup.sh run-swan' >> $outfile
echo 'export LOCAL=$CERNBOX_HOME/.local/$(basename $REPO_ROOT)' >> $outfile
echo 'export PYTHON_LOCAL=$LOCAL/lib/python2.7/site-packages/' >> $outfile
echo 'export PATH=$PATH:$LOCAL/bin:$OLD_PATH' >> $outfile 
echo 'export PYTHONPATH=$PYTHON_LOCAL:$PYTHONPATH' >> $outfile
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OLD_LD_LIBRARY_PATH' >> $outfile
echo 'Done! You can now use SWAN for this analysis.'

