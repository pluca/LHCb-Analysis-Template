## Make a kerberos key tab (on lxplus)

N.B.: You can avoid this step by putting just "kinit {user}" inside run_kinit.sh.
But then you will be asked your password many times.

```
>> ktutil
ktutil:  add_entry -password -p {user}@CERN.CH -k 1 -e arcfour-hmac-md5
Password for {user}@CERN.CH: 
ktutil:  add_entry -password -p {user}@CERN.CH -k 1 -e aes256-cts
Password for {user}@CERN.CH: 
ktutil:  wkt mykey.keytab
```

You may also try this shortcut but it does not always work.

```
ktutil -k mykey.keytab add -p {your cern user}@CERN.CH -e arcfour-hmac-md5 -V 1
```


- Copy mykey.keytab into the folder with the Dockerfile

N.B.: Do not change name to it or you'll have to change it into scripts/runkinit.sh as well.

--------

## Build the image and run the analysis

- Put your id_rsa in the Docker folder. The one withOUT .pub. N.B.: The key registered with gitlab.

N.B.: This will only work if when you created the key you put no passphrase.

- Fill the Dockerfile

```
python fillDockerfile.py ## Will help you fill your info into the Dockerfile
```

Furthermore if you want to use the images as a runner you have to remove the last lines of the Dockerfile (see in the Dockerfile at the end).
Otherwise you have to fill in the git url.

- Go into the Docker folder.

```
docker build -t {name} .
docker run -i -t {name}
```

The image building will take ~20 min. Only needed once.

Enjoy!

