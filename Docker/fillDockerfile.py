import os, sys

tmp = open("Dockerfile.tmp").read()

d = {}
print "I'll ask you some information to automatically fill a customised Dockerfile for you"
d['user'] = raw_input('Your lxplus user:  ')
d['mail'] = raw_input('Your cern email:  ')
d['name'] = raw_input('Your full name:  ')
d['repo'] = raw_input('Your ssh repository url. If you do not have one just type enter and the template repository will be added:  ')

if d['repo']=='' : d['repo']='ssh://git@gitlab.cern.ch:7999/pluca/LHCb-Analysis-Template.git'
import re, sys
main = re.findall('/([^/]+?)\.git',d['repo'])
if len(main) < 1 : sys.exit()
d['main'] = main[0]

print d

dockerfile = tmp.format(**d)
out = open("Dockerfile","w")
out.write(dockerfile)
out.close()

script = open("scripts/run_kinit.tmp").read().format(**d)
out = open("scripts/run_kinit.sh","w")
out.write(script)
out.close()
