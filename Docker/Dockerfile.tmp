FROM rootproject/root-ubuntu16

MAINTAINER Luca Pescatore <luca.pescatore@cern.ch>

# install basic libraries
USER root

## Install extra packages
RUN apt-get -y update \
    && apt-get -y install wget

RUN apt-get -y update \
    && apt-get -y install build-essential libx11-dev libxpm-dev libxft-dev libxext-dev libpng12-dev libjpeg-dev gfortran libssl-dev libpcre3-dev libgl1-mesa-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev libcfitsio3-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev libxml2-dev libafterimage0 libafterimage-dev cmake vim emacs git zsh 

RUN apt-get -y update && apt-get -y install krb5-user openssh-server krb5-config

RUN apt-get -y update \
    && apt-get -y install libboost-all-dev

# gsl and clang
RUN apt-get -y update && apt-get -y upgrade \
    && apt-get -y install libgsl2 clang
#    && apt-get -y install libgsl0ldbl libgsl0-dev clang

# XRootD
RUN git clone --depth 1 http://github.com/xrootd/xrootd.git -b v4.7.1 --single-branch \
    && mkdir xrootd-build \
    && cd xrootd-build \
    && cmake ../xrootd -DCMAKE_INSTALL_PREFIX=/usr/local -DENABLE_PERL=FALSE -DENABLE_FUSE=FALSE -DENABLE_KRB5=TRUE \
    && make && make install \
    && cd .. && rm -rf xrootd xrootd-build

RUN apt-get -y update \
    && apt-get -y install graphviz exiv2 exiftool

ENV LD_LIBRARY_PATH /usr/local/lib:$LD_LIBRARY_PATH
ENV PYTHONPATH /usr/local/lib:$PYTHONPATH


# install conda to manage auxilliary packages
ADD scripts/setup_snake.sh /root/setup_snake.sh
RUN chmod +x /root/setup_snake.sh
RUN /root/setup_snake.sh

ENV PATH /opt/conda/bin:$PATH
RUN pip install virtualenv virtualenvwrapper

## Configuration scripts
ADD krb5.conf /etc/krb5.conf
ADD mykey.keytab /root/mykey.keytab
ADD scripts/run_kinit.sh /root/run_kinit.sh

# in order to be able to push to a repository you need to set your git identity
RUN git config --global user.email "{mail}" && \
    git config --global user.name "{name}" && \
    git config --global push.default simple


# Add authentication files (add more if needed)
RUN mkdir /root/.ssh
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh
RUN chmod 700 /root/.ssh/id_rsa
# add them to the ssh configuration
RUN echo "Host gitlab.cern.ch\nIdentityFile ~/.ssh/id_rsa\nStrictHostKeyChecking=no\nUser {user}" > /etc/ssh/ssh_config

### Specific to your repo. N.B.: This is not needed for the image used to build the CI runner

RUN git clone --recursive {repo}

ENV ANAROOT /root/{main}
ADD scripts/anasetup.sh $ANAROOT/anasetup.sh
RUN cd $ANAROOT && chmod +x $ANAROOT/anasetup.sh && /bin/bash $ANAROOT/anasetup.sh

CMD ["bash"]




