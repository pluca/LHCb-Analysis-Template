import analysis as an
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("outfile")
args = parser.parse_args()

## Save some numbers in the DB and dump it to memory
an.db.update({'thing1_sample1' : 54, 'thing1_sample2' : 108, 'thing2_sample2' : 200})
an.dumpDB(an.db)
print an.db

## Fill a template. N.B.: If no db is specified the an.db will be used.
an.outfiles.fill_template(args.outfile,'example.tmp',an.db)


