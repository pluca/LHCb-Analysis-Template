################## Location in repository or on EOS

import os
repo = os.getenv('ANAROOT')

class loc : pass
loc.ROOT       = repo+'/'
loc.OUT        = loc.ROOT+'out/'
loc.PYTHON     = loc.ROOT+'python/'
loc.PLOTS      = loc.OUT+'plots/'
loc.LHCB       = loc.ROOT+'LHCb/'
loc.TMPS       = loc.ROOT+'templates/'
loc.TABS       = loc.OUT+'tables/'
loc.LOGS       = loc.OUT+'/logs/'
loc.WGEOS      = "/eos/lhcb/wg/{YOUR WG}/{YOUR PROJECT}" # Check that you have writing permission here!

### Raw data locations

dataids = { 

        'CL11'        :(loc.WGEOS, ['MU','MD']),
        'CL12'        :(loc.WGEOS, ['subfolder1','subfolder2'])

            ## Add here all your samples data and MC
          }


