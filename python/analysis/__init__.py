import os, sys, glob

import warnings
warnings.filterwarnings( action='ignore', category=RuntimeWarning, message='creating converter.*' )
warnings.filterwarnings( action='ignore', category=RuntimeWarning, message='from ROOT import.*' )

if os.getenv('ANAROOT') is None :
    print "Attention, you did not setup. Run 'source setup.sh' before doing anything"
    sys.exit()

import ROOT
import cuts
from locations import loc, dataids
from outfiles import dumpDB, initDB, loadDB, outfiles
initDB()
db = loadDB()

from pyutils.scripts import remotels, checkbatch
from pyutils.LHCb.LHCbStyle import set_lhcbStyle
set_lhcbStyle()


repo = os.getenv('ANAROOT')
ROOT.gROOT.ProcessLine('.x '+repo+'/pyutils/LHCb/lhcbStyle.C')



